#Version 0.25.0 (2024-12-09)
Change from print statements to python logger for logging. This also logs to stderr, which makes OkapiCli more useful in scripts.
Add virtual "upsert" method for "insert or update" objects by id.

#Version 0.24.0 (2024-11-22)
Added verbose messages for login problems
Added module for mod_configuration

#Version 0.23.0 (2024-11-22)
fix Docker build

#Version 0.22.0 (2024-10-04)
Moved configuration of ApiClient class from a static to an instance variable 

#Version 0.21.0 (2024-07-29)
Re-added clear expired tokens

#Version 0.20.0 (2024-07-29)
Added debug switch
Fix refreshing token

#Version 0.19.0 (2024-05-29)
Fixed Login Bug for non-Refresh-Token systems introduced in the last version.

#Version 0.18.0 (2024-05-24)
Add Refresh Token Rotation. Bump Version to 0.18 to align to internal docker image versions.

#Version 0.0.8 (2024-01-24)
Fix trailing slashes in URL

#Version  0.0.7 (2024-01-12)
Removed Refreshtoken for now, does not work with Poppy

#Version 0.0.6 (2023-08-10)

Fix double slashes. https://issues.folio.org/browse/FOLIO-3864

#Version 0.0.5 (2023-07-26)

Add --data, fix delete without body data

#Version 0.0.4 (2023-07-03)

Enable body data for delete

#Version 0.0.3 (2021-02-26)

New commands for the core module.

#Version 0.0.2

Start integration of module commands. Direct API calls have to be made using the "raw" module.

#Version 0.0.1

Initial Release
