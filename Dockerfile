FROM python:3

WORKDIR /usr/src/app

# COPY requirements.txt ./
# RUN pip install --no-cache-dir -r requirements.txt

COPY . .
COPY sources.list /etc/apt/sources.list
RUN python -m pip install .
RUN apt update
RUN apt install -y jq

