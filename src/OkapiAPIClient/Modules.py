from OkapiAPIClient.ApiClient import ApiClient

import json
import logging

logger = logging.getLogger(__name__)




class OkapiCoreModule(ApiClient):

    def listNodes(self):
        return self.makeAPICall('/_/discovery/nodes')

    def listModules(self):
        return self.makeAPICall('/_/proxy/modules')

    def health(self):
        return self.makeAPICall('/_/discovery/health')

    def getDeployment(self,instId=None,srvcId=None,nodeId=None):

        if instId is not None and self.isUUID(instId):
            return self.makeAPICall('/_/deployment/modules/'+instId)
        response = self.makeAPICall('/_/deployment/modules')
        # filter content if some ids are given

        if instId is not None:
            response.addFilter('instId',instId)
        if srvcId is not None:
            response.addFilter('srvcId',srvcId)
        if nodeId is not None:
            response.addFilter('nodeId',nodeId)
        response.applyFilters()
        return response

    def stopModules(self,instId=None,srvcId=None,nodeId=None):
        if instId is not None and self.isUUID(instId):
            return self.makeAPICall('/_/deployment/modules/'+instId, 'delete')
        response = self.getDeployment(instId,srvcId,nodeId)
        ret = []
        for val in response.filteredParsed:
            ret.append(self.makeAPICall('/_/deployment/modules/'+val['instId'], 'delete'))
        return ret

    def deployModule(self):
        return self.makeAPICall('/_/deployment/modules/', 'post')

    def pullModules(self):
        return self.makeAPICall('/_/proxy/pull/modules', 'post', '{"urls": ["https://folio-registry.dev.folio.org"]}')




class FolioUsersModule(ApiClient):

    def listUsers(self):
        return self.makeAPICall('/users?limit=2147483647')
    
class ModConfigModule(ApiClient):
    def listConfigs(self,module=None, name=None, code=None):
        queries = []
        if module is not None:
            queries.append('module=='+module)
        if name is not None:
            queries.append('configName=='+name)
        if code is not None:
            queries.append('code=='+code)

        apiquery='/configurations/entries?limit=2147483647'
        if queries:
            apiquery+='&query='+' and '.join(queries)
        return self.makeAPICall(apiquery)
    
    def setConfig(self,module=None, name=None, code=None, value=None, enabled=True):
        # check if we have a new config or if we have to change the config
        result = json.loads(self.listConfigs(module,name,code).filteredJson)
        # pprint.pprint(result)
        if result['configs']:
            # get Id
            id=result['configs'][0]['id']
            # Update
            data=result['configs'][0]
            data['value']=value
            return self.makeAPICall('/configurations/entries/'+id, 'put', json.dumps(data))
        else:
            data={}
            data['value']=value
            data['module']=module
            data['code']=code
            data['configName']=name
            return self.makeAPICall('/configurations/entries','post',json.dumps(data))

            
