from getpass import getuser
import http.cookiejar
from json import JSONDecodeError
from os import makedirs, path
from os.path import dirname

import json
import re
# from pprint import pprint

import requests
import http
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import logging

logger = logging.getLogger(__name__)



# from cgi import parse_header


class ExtendedResponse(requests.Response):
    """Extends Response class from requests by a filter for Json Objects. Not applicable for non-json content"""
    responseObject = None
    responseFilter = []
    filtersApplied = False
    filteredJson = ""
    filteredParsed = []

    def addFilter(self, item: str, pattern: str):
        self.responseFilter.append({'item': item,
                                    'pattern': pattern})
        self.filtersApplied = False

    def applyFilters(self):
        if self.filtersApplied is True:
            return
        if len(self.responseFilter) == 0:
            try:
                self.filteredJson = self.text
                self.filteredParsed = json.loads(self.text)
            except JSONDecodeError:  # mostly empty response
                pass
            return
        parsed = json.loads(self.text)
        self.filteredParsed = []
        for item in parsed:
            good = True
            for rFilter in self.responseFilter:
                if item[rFilter['item']].find(rFilter['pattern']) == -1:
                    good = False
            if good:
                self.filteredParsed.append(item)

        self.filteredJson = json.dumps(self.filteredParsed, ensure_ascii=False)
        self.filtersApplied = True

    # TODO: Add output format creation to ExtendedResponse


class ApiClient:

    def __init__(self, url=None, username=None, password=None, tenant=None, insecure=None, token_cache_method=None,
                 token_cache_file=None, verbose=None, debug=None):
        
        # default config
        self.configuration = {
            'url': 'http://localhost.9130',
            'username': 'default_username',
            'password': 'default_password',
            'tenant': 'default_tenant',
            'insecure': False,
            'token_cache_method': 'file',
            'token_cache_file': path.expanduser('~')+'/.OkapiCLITokenCache/__URL__/__TENANT__/__USERNAME__/token.txt',

        }
        self.okapiToken = None
        self.tokenJar = None

        if url is not None:
            #if not validators.url(url):
            #    raise ValueError('Given URL is not valid')
            self.configuration['url'] = url
        if username is not None:
            self.configuration['username'] = username
        if password is not None:
            self.configuration['password'] = password
        if tenant is not None:
            self.configuration['tenant'] = tenant
        if insecure is not None:
            self.configuration['insecure'] = insecure
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        else:
            self.configuration['insecure'] = False
        if token_cache_method is not None:
            if token_cache_method in ('file',):
                self.configuration['token_cache_method'] = token_cache_method
            else:
                raise ValueError('Invalid token_cache_method value')
        if token_cache_file is not None:
            self.configuration['token_cache_file'] = token_cache_file
        if verbose is not None:
            self.configuration['verbose'] = bool(verbose)
        else:
            self.configuration['verbose'] = False
        if debug is not None:
            self.configuration['debug'] = bool(verbose)
        else:
            self.configuration['debug'] = False
        # Get Auth Token from Cache or from Okapi
        if not self.readTokenCache():
            try:
                self.getAuthTokenWithExpiry()
            except PermissionError:
                self.getAuthToken()
            except:
                raise PermissionError('Unable to log into Okapi')
            
    def strtobool(self, s):
        if s.lower() in ('true', 'yes', 'y', '1'):
            return True
        if s.lower() in ('false', 'no', 'n', '0'):
            return False
        raise ValueError('Unknown boolean value given')

    # Todo: Refresh Token with refreshtoken?
    # refreshtoken not yet implemented. see https://wiki.folio.org/pages/viewpage.action?pageId=36574669

    def handleAuthError(self, authresponse):
        message = 'Unable to log into Okapi'
        if self.configuration['verbose']:
            if authresponse.status_code != 403:
                text = json.loads(authresponse.text)
                okapimsg = text['errors'][0]['message']
                message += f": {authresponse} {okapimsg}"
            else:
                message += ": " + str(authresponse)
        raise PermissionError(message)
    
    def getAuthToken(self):
        logger.debug("Getting old style auth token")
        authData = {
            'username': self.configuration['username'],
            'password': self.configuration['password']
        }

        authresponse = requests.post(url=self.configuration['url'] + '/authn/login',
                                     headers={
                                         'X-Okapi-Tenant': self.configuration['tenant'],
                                     },
                                     json=authData,
                                     verify=not self.configuration['insecure']
                                     )
        if 'x-okapi-token' in authresponse.headers.keys():
            logger.debug("Got valid old style token")
            self.okapiToken = authresponse.headers['x-okapi-token']
            self.cacheToken()
        else:
            self.handleAuthError(authresponse)

    def getAuthTokenWithExpiry(self):
        logger.debug("Trying to get token with expiry")
        authData = {
            'username': self.configuration['username'],
            'password': self.configuration['password']
        }

        authresponse = requests.post(url=self.configuration['url'] + '/authn/login-with-expiry',
                                     headers={
                                         'X-Okapi-Tenant': self.configuration['tenant'],
                                     },
                                     json=authData,
                                     verify=not self.configuration['insecure']
                                     )
        
        self.tokenJar = http.cookiejar.LWPCookieJar(self._getCacheFileName())
        if 'folioAccessToken' in authresponse.cookies.keys() and 'folioRefreshToken' in authresponse.cookies.keys():
            logger.debug("Got valid token and refreshtoken")
            for cookie in authresponse.cookies:
                self.tokenJar.set_cookie(cookie)
            self.tokenJar.save()
        else:
            self.handleAuthError(authresponse)

    def tryRefreshToken(self):
        logger.debug("Trying to refresh the token")
        self.tokenJar.clear_expired_cookies()
        cookienames = requests.utils.dict_from_cookiejar(self.tokenJar).keys()
        if 'folioRefreshToken' not in cookienames:
            logger.debug("Unable to refresh token, no valid refreshtoken")
            return False
        if 'folioAccessToken' in cookienames:
            logger.debug("Not needed, token is still valid")
            return True

        # we have a refresh token and we dont have a valid authtoken.
        authresponse = requests.post(url=self.configuration['url'] + '/authn/refresh',
                                     headers={
                                         'X-Okapi-Tenant': self.configuration['tenant'],
                                         'Content-type': 'application/json'
                                     },
                                     json='{"tokenTransport": "cookie"}',
                                     verify=not self.configuration['insecure'],
                                     cookies=requests.utils.dict_from_cookiejar(self.tokenJar)
                                     )
        if 'folioAccessToken' in authresponse.cookies.keys() and 'folioRefreshToken' in authresponse.cookies.keys():
            logger.debug("Token successfully refreshed")
            for cookie in authresponse.cookies:
                self.tokenJar.set_cookie(cookie)
            self.tokenJar.save()
        else:
            logger.debug(authresponse.text)
            logger.debug("Refreshing the token does not work. Just trying to get fresh tokens")
            self.getAuthTokenWithExpiry()
            # raise PermissionError('Unable to refresh the AuthToken')

        


    def cacheToken(self):
        logger.debug("Cacching old style token")
        filename = self._getCacheFileName()
        # old style tokens
        f = open(filename, mode='w')
        tokens = {'okapiToken': self.okapiToken}
        f.write(json.dumps(tokens))
        f.close()
       


    def readTokenCache(self):
        filename = self._getCacheFileName()
        logger.debug("reading Token Cache from file "+filename)
        if not path.isfile(filename):
            makedirs(dirname(filename), exist_ok=True)
            return False
        try:
            self.tokenJar = http.cookiejar.LWPCookieJar(filename)
            self.tokenJar.load()
            logger.debug("Token cache file is a cookie jar")
            if "folioRefreshToken" not in requests.utils.dict_from_cookiejar(self.tokenJar):
                logger.debug("... but does not contain a refresh token!")
                return False 
            return True
        except http.cookiejar.LoadError as e:
            f = open(filename, mode='r')
            tokens = json.loads(f.read())
            self.okapiToken = tokens['okapiToken']
            f.close()
            logger.debug("token cache file os old style")
            return True
        except Exception as e:     
            logger.verbose("Got Exception: " + str(e))
        logger.debug("No Token cache file")
        return False

    def _getCacheFileName(self):
        return path.normcase(path.normpath(self.configuration['token_cache_file'].
                             replace('__OSUSERNAME__', getuser()).
                             replace('__TENANT__', self.configuration['tenant']).
                             replace('__USERNAME__', self.configuration['username']).
                             replace('__URL__', self.configuration['url'].replace(':', '-').replace('/', '-'))))

    def makeAPICall(self, path: str, method='get', data=None, contenttype='application/json; charset=utf-8',
                    customheaders=None):
        self.tryRefreshToken()
        addslash=''
        if self.configuration['url'].endswith('/') is False and path.startswith('/') is False:
            addslash = '/'
        url = self.configuration['url'] + addslash + path
        # charset = parse_header(contenttype)["charset"]
        if data is not None:
            data = str(data).encode('utf-8')
        headers = {
            'X-Okapi-Tenant': self.configuration['tenant'],
            'x-okapi-token': self.okapiToken,
            'Content-type': contenttype,
        }
        # add custom headers; but take care to not overwrite defaults! 
        if not customheaders:
            customheaders = {}
        for header in customheaders:
            name, value = header.split(':', 1)
            if name not in headers:
                headers[name.strip()] = value.strip()
        # ensure utf-8 content
        verify = not self.configuration['insecure']
        response = None
        cookies=requests.utils.dict_from_cookiejar(self.tokenJar)
        if method == 'get':
            response = requests.get(url=url, headers=headers, verify=verify, cookies=cookies)
        if method == 'post':
            response = requests.post(url=url, headers=headers, verify=verify, data=data, cookies=cookies)
        if method == 'put':
            response = requests.put(url=url, headers=headers, verify=verify, data=data, cookies=cookies)
        if method == 'delete':
            if data:
                response = requests.delete(url=url, headers=headers, verify=verify, data=data, cookies=cookies)
            else:
                response = requests.delete(url=url, headers=headers, verify=verify, cookies=cookies)
        if method == 'upsert':
            if data:
                jsondata = json.loads(data)
                if jsondata['id']:
                    idresponse = requests.get(url=url + "/" + jsondata['id'], headers=headers, verify=verify, cookies=cookies)
                    if idresponse.status_code == 200:
                        logger.debug("Found entry with id")
                        # compare data
                        if jsondata.items() <= idresponse.json().items():
                            logger.info("No update needed")
                            response=idresponse # needed to avoid errors below
                        else:
                            logger.info("Updating entry")
                            response = requests.put(url=url + "/" + jsondata['id'], headers=headers, verify=verify, data=data, cookies=cookies)
                    elif idresponse.status_code == 404:
                        logger.info("Entry with id not found. Creating new one")
                        response = requests.post(url=url, headers=headers, verify=verify, data=data, cookies=cookies)
                    else:
                        raise ValueError("Unexpected status code fetching id")
                else:
                    raise ValueError("Upsert input data need a 'id' field")
            else:
                raise ValueError("Upsert method needs input data")
        if response is None:
            raise ValueError("Unknown request method")
        if response.status_code == 401 and response.text.lower().startswith("invalid token"):
            # Token is expired or invalid. Try to re-authenticate:
            try:
                self.getAuthToken()
                response = self.makeAPICall(path, method, data)
            except Exception as e:
                logger.verbose("Got Exception: " + str(e))
                raise PermissionError(
                    'Auth Token expired or invalid. Refreshing token failed. You probably have to use --password')
        if response.status_code > 299:
            if self.configuration['verbose']:
                raise PermissionError(
                    'Got HTTP Return Code ' + str(response.status_code) + '\n\nResponse Headers:\n' + str(
                        response.headers) + '\n\nResponse Body:\n' + str(response.text))
            else:
                raise PermissionError('Got HTTP Return Code ' + str(response.status_code))
        logger.info("Response Status: "+str(response.status_code))
        # print(response.headers)
        response.__class__ = ExtendedResponse
        response.applyFilters()
        return response

    def isUUID(self, s: str) -> bool:
        # https://dev.folio.org/guides/uuids/
        if re.match("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[1-5][a-fA-F0-9]{3}-[89abAB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$",
                    s) == None:
            return False
        return True
