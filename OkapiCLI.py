#!/usr/bin/env python3

from OkapiAPIClient.ApiClient import ApiClient
import argparse
import configparser
import json
import sys
from pprint import pprint
import traceback
import csv
import io
import logging

logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(
    description="Okapi Command Line Interface", prog="OkapiCLI.py"
)

parser.add_argument(
    "--config",
    "-c",
    dest="configfile",
    default=".okapicli",
    help="Read options from a config file. Defaults to .okapicli",
)
parser.add_argument(
    "--url",
    "-u",
    dest="url",
    help="Base URL to connect to Okapi, for example http://okapi.example.com:9130",
)
parser.add_argument("--username", "-U", dest="username", help="Username")
parser.add_argument("--password", "-p", dest="password", help="password")
parser.add_argument("--tenant", "-t", dest="tenant", help="Tenant")
parser.add_argument(
    "--insecure",
    "-i",
    dest="insecure",
    action="store_true",
    help="if set, then invalid https certificates are accepted",
)

parser.add_argument(
    "--outformat",
    "-o",
    dest="outformat",
    default="json",
    choices=["json", "python", "csv"],
    help="Output Format: json(default), python (pprint a dictionary), csv",
)
parser.add_argument(
    "--verbose", "-v", dest="verbose", action="store_true", help="verbose"
)
parser.add_argument(
    "--debug", "-d", dest="debug", action="store_true", help="Show debugging info"
)

subparsers = parser.add_subparsers(
    help="Modules. Use -h to get module help", title="Modules", dest="module"
)

# Core
coreParser = subparsers.add_parser("core", help="Core module (nodes, modules, tenants)")
coreParser.add_argument(
    "--list-nodes", dest="listnodes", action="store_true", help="List Okapi nodes"
)
coreParser.add_argument(
    "--list-modules",
    dest="listmodules",
    action="store_true",
    help="List Okapi Modules from proxy " "service (/_/proxy/modules)",
)
coreParser.add_argument(
    "--health",
    dest="health",
    action="store_true",
    help="Show health status of all modules",
)
coreParser.add_argument(
    "--get-deployment",
    dest="getdeployment",
    action="store_true",
    help="Get deployment "
    "descriptors of modules, "
    "can be combined with "
    "--svcId. --instId, "
    "--nodeId",
)
coreParser.add_argument(
    "--srvcId",
    dest="srvcId",
    default=None,
    help='Specify service Id like "mod-template-engine-1.12.0" or "mod-template-engine*"',
)
coreParser.add_argument(
    "--instId",
    dest="instId",
    default=None,
    help='Specify instance Id like "135d1351-2ab2-4b20-962b-acf60ca89042"',
)
coreParser.add_argument(
    "--nodeId",
    dest="nodeId",
    default=None,
    help='Specify node Id like "192.168.128.54"',
)
coreParser.add_argument(
    "--stop-modules",
    dest="stopmodules",
    action="store_true",
    help="Stop modules, can be combined with --svcId. --instId, --nodeId",
)
coreParser.add_argument(
    "--deploy-module",
    dest="deploymodule",
    action="store_true",
    help="Deploy a module. Deployment descriptor is read from Stdin.",
)
coreParser.add_argument(
    "--pull-modules",
    dest="pullmodules",
    action="store_true",
    help="Pulls new module information from folio-registry.dev.folio.org.",
)

# Users
usersParser = subparsers.add_parser("users", help="Users module")
usersParser.add_argument(
    "--list-users", dest="listusers", action="store_true", help="List users of a tenant"
)

# Raw
rawParser = subparsers.add_parser("raw", help="for sending raw API requests")
rawParser.add_argument(
    "--method",
    "-m",
    dest="method",
    default="get",
    choices=["get", "post", "put", "delete", "upsert"],
    help="Method for raw API calls: get(default), post, put, upsert or delete. "
    "Data to send via delete, post, put or upsert are read from stdin or given using the --data option"
    "Upsert is not a native method, if first cheks if there is a entry with the given id"
    "and if not, it creates the entry. If the id is present, it changes the entry if data differ.",
)
rawParser.add_argument(
    "--content-type", dest="contenttype", default="application/json; charset=utf-8"
)
rawParser.add_argument(
    "--rawpath",
    "-r",
    dest="rawpath",
    help="Raw Url Path for the API Call (may include parameters)",
)
rawParser.add_argument(
    "--custom-header",
    "-x",
    dest="customheaders",
    action="append",
    help="Add a custom HTTP header (repeatable)",
)
rawParser.add_argument(
    "--data",
    "-d",
    dest="data",
    default=None,
    nargs="?",
    help="Data for post, put or delete",
)
rawParser.add_argument(
    "--file",
    "-f",
    dest="file",
    default=None,
    nargs="?",
    help="File containing data for post, put or delete",
)

# Configuration
modconfigParser = subparsers.add_parser(
    "modconfig", help="Read/Write to mod_configuration"
)
modconfigParser.add_argument(
    "--list",
    "-L",
    dest="mc_list",
    action="store_true",
    help="List all configs from mod_configuration",
)
modconfigParser.add_argument(
    "--module", "-m", dest="mc_module", help="Match configs from module"
)
modconfigParser.add_argument(
    "--name", "-n", dest="mc_name", help="Match configs from config name"
)
modconfigParser.add_argument(
    "--code", "-c", dest="mc_code", help="Match configs from config code"
)
# modconfigParser.add_argument('--enabled', "-e", dest='mc_enabled', action='store_true', help='Enable config (default)')
# modconfigParser.add_argument('--disabled', "-d", dest='mc_enabled', action='store_false', help='Disable config')
modconfigParser.add_argument(
    "--value", "-V", dest="mc_value", default="none", help="Value"
)
modconfigParser.add_argument(
    "--set",
    "-S",
    dest="mc_set",
    action="store_true",
    help="Set config. Needs module, name, code and value",
)
# modconfigParser.add_argument('--delete', "-D", dest='mc_delete', action='store_true', help='Delete config. Needs module, name, code and value')


args = parser.parse_args()
# read config file
config = configparser.ConfigParser()
config.read(args.configfile)

loglevel = logging.WARNING

if args.verbose:
    loglevel = logging.INFO

if args.debug:
    args.verbose = True
    loglevel = logging.DEBUG


logging.basicConfig(level=loglevel, format='%(levelname)s: %(message)s')

if not args.url:
    try:
        args.url = config["okapicli"]["url"]
    except Exception as err:
        logger.error("Missing URL Parameter")
        sys.exit(1)

if not args.username:
    try:
        args.username = config["okapicli"]["username"]
    except Exception as err:
        logger.error("Missing Username Parameter")
        sys.exit(1)

if not args.password:
    try:
        args.password = config["okapicli"]["password"]
    except Exception as err:
        pass

if not args.tenant:
    try:
        args.tenant = config["okapicli"]["tenant"]
    except Exception as err:
        logger.error("Missing Tenant Parameter")
        sys.exit(1)



try:
    rawresult = ""
    subobject = None

    # Raw
    if args.module == "raw":
        client = ApiClient(
            url=args.url,
            username=args.username,
            password=args.password,
            tenant=args.tenant,
            insecure=args.insecure,
            verbose=args.verbose,
            debug=args.debug,
        )
        data = None
        if args.method in ["post", "put", "delete", "upsert"]:
            if args.file:
                data = open(args.file, "r", encoding="utf-8").read()
            elif args.data:
                data = args.data
            elif not sys.stdin.isatty():
                input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding="utf-8")
                data = input_stream.read()
        rawresult = client.makeAPICall(
            args.rawpath,
            method=args.method,
            data=data,
            contenttype=args.contenttype,
            customheaders=args.customheaders,
        ).filteredJson

    # Core
    if args.module == "core":
        from OkapiAPIClient.Modules import OkapiCoreModule

        client = OkapiCoreModule(
            url=args.url,
            username=args.username,
            password=args.password,
            tenant=args.tenant,
            insecure=args.insecure,
            verbose=args.verbose,
        )
        if args.listnodes:
            rawresult = client.listNodes().text
        elif args.listmodules:
            rawresult = client.listModules().text
        elif args.health:
            rawresult = client.health().text
        elif args.getdeployment:
            rawresult = client.getDeployment(
                args.instId, args.srvcId, args.nodeId
            ).filteredJson
        elif args.stopmodules:
            rawresult = client.stopModules(
                args.instId, args.srvcId, args.nodeId
            ).filteredJson
        elif args.pullmodules:
            rawresult = client.pullModules().text

    # Users
    if args.module == "users":
        from OkapiAPIClient.Modules import FolioUsersModule

        client = FolioUsersModule(
            url=args.url,
            username=args.username,
            password=args.password,
            tenant=args.tenant,
            insecure=args.insecure,
            verbose=args.verbose,
        )
        if args.listusers:
            rawresult = client.listUsers().filteredJson
            subobject = "users"

    if args.module == "modconfig":
        from OkapiAPIClient.Modules import ModConfigModule

        client = ModConfigModule(
            url=args.url,
            username=args.username,
            password=args.password,
            tenant=args.tenant,
            insecure=args.insecure,
            verbose=args.verbose,
        )

        if args.mc_list:
            rawresult = client.listConfigs(
                args.mc_module, args.mc_name, args.mc_code
            ).filteredJson
            subobject = "configs"

        if args.mc_set:
            if not args.mc_value:
                print("Missing value")
                sys.exit(1)
            rawresult = client.setConfig(
                args.mc_module, args.mc_name, args.mc_code, args.mc_value
            ).filteredJson

    if len(rawresult) > 0:
        result = json.loads(rawresult)
        if subobject is not None:
            result = result[subobject]
        if args.outformat == "python":
            pprint(result)
        if args.outformat == "json":
            print(json.dumps(result, indent=4, sort_keys=True, ensure_ascii=False))
        if args.outformat == "csv":
            out = csv.writer(sys.stdout)
            for r in result:
                out.writerow(r.values())


except Exception as err:
    logger.debug(traceback.format_exc())
    logger.error(err)

    sys.exit(1)
