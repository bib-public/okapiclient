﻿from setuptools import setup, find_packages

setup(
        name="pyOkapiClient"
        , version="0.25.0"
        , description=""
        , packages=find_packages(where="./src")
        , package_dir={"OkapiAPIClient": "src/OkapiAPIClient"}
        , install_requires=["pyopenssl", "validators", "argparse", "configparser", "requests"]
        , author="Florian Gleixner"
        , python_requires=">3.3"
        , url="https://gitlab.bib-bvb.de/folio-public/okapiclient/-/tree/master/"
        )
