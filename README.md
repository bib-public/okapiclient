# OkapiCLI.py

OkapiCLI.py is a command line tool, that can be used to send API calls to Okapi, the core of Folio.
See https://dev.folio.org/reference/api/ for possible APIs.

## Installation

Linux users:

```bash
git clone <repository url>
cd okapiclient
pipenv shell
python3 -m pip install .
./OkapiCLI.py --help
```

Windows users with pipenv:

Install python 3 for all users. Remember path to python.exe, if you have another installation (standard e.g.: `C:\Program Files\Python39\python.exe`).

```powershell
git clone <repository url>
cd okapiclient
pipenv --python "C:\Program Files\Python39\python.exe" shell
python3 -m pip install .
python .\OkapiCLI.py --help
```

Windows Users should:

* Install latest Python from Windows App Store
* Start a cmd shell
* `git clone <repository url>`
* `python -m venv okapiclient`
* `okapiclient\Scripts\activate`
* `cd okapiclient`
* `pip install setuptools`
* `python setup.py install`
* I had to install VisualC++ Compile Environment
* If setup.py fails, try to install libraries with pip install like:
  * `pip install pyopenssl`
  * ... (see setup.py)
* run OkapiCLI with python like `python OkapiCLI.py --help`
* `Scripts\activate` has to be run to set environment, so run it, if you restarted windows


## Authentication

The first time you use OkapiCLI, you have to provide a Password with `--password`. OkapiCLI.py saves the auth token to a file in `.OkapiCLITokenCache/$URL/$TENANT/$OKAPIUSER/token.txt` in `$USERPROFILE` on Windows (e.g. `C:\Users\WinUser`) and `$HOME` on Linux (e.g. `/home/linuxuser`). You should delete the token files, if the computer is used by other persons too. Once a token file is present, `--password` can be omitted.

## Modules

Folio Modules can be accessed by module Options in OkapiCLI. The "raw" module can be used to create any API call

## Usage

### Example: Use the raw module to list users with username containing the string diku

```bash
./OkapiCLI.py --url https://my-installed-folio.org/okapi --username diku_admin --password admin --tenant diku raw --rawpath "/users?query=username=diku"
```

### Example: Use the users module to list all users

```bash
./OkapiCLI.py --url https://my-installed-folio.org/okapi --username diku_admin --password admin --tenant diku users --list-users
```

### Help

```bash
./OkapiCLI.py --help
usage: OkapiCLI.py [-h] [--url URL] [--username USERNAME]
                   [--password PASSWORD] [--tenant TENANT] [--insecure]
                   [--outformat {json,python,csv}] [--verbose] [--debug]
                   {core,users,raw} ...

Okapi Command Line Interface

optional arguments:
  -h, --help            show this help message and exit
  --url URL, -u URL     Base URL to connect to Okapi, for example
                        http://okapi.example.com:9130
  --username USERNAME, -U USERNAME
                        Username
  --password PASSWORD, -p PASSWORD
                        password
  --tenant TENANT, -t TENANT
                        Tenant
  --insecure, -i        if set, then invalid https certificates are accepted
  --outformat {json,python,csv}, -o {json,python,csv}
                        Output Format: json(default), python (pprint a
                        dictionary), csv
  --verbose, -v         verbose
  --debug, -d           Show more debugging output

Modules:
  {core,users,raw}      Modules. Use -h to get module help
    core                Core module (nodes, modules, tenants)
    users               Users module
    raw                 for sending raw API requests

```

To get help for specific modules use:

```bash
./OkapiCLI.py users --help
usage: OkapiCLI.py users [-h] [--list-users]

optional arguments:
  -h, --help    show this help message and exit
  --list-users  List users of a tenant
```

### Changing data

Data sent via --method put or post have to be json encoded and are read from stdin. You can use files with json content and use a pipe:

```bash
cat file | ./OkapiCLI.py ....
```

or echo the content and pipe it to OkapiCLI.py:

```bash
echo '{                                                                                                                                             
     "module": "SMTP_SERVER",
     "configName": "smtp",
     "code": "EMAIL_SMTP_HOST",
     "description": "my server smtp host",
     "default": true,
     "enabled": true,
     "value": "mailhost.my-domain.de"
}' | ./OkapiCLI.py --url https://my-installed-folio.org/okapi --username diku_admin  --tenant diku  --insecure raw --rawpath "/configurations/entries" --method post
```

The special method upsert can be used to update or insert data by id. For example:

```
echo '{
            "id": "30b3e36a-d3b2-415e-98c3-47fbdf878862",
            "name": "videotest recording"
}' | ./OkapiCLI.py --url https://my-installed-folio.org/okapi --username diku_admin  --tenant diku raw -r "/material-types" -m upsert
```
inserts the material-type or changes it, if a material-type with the given id already exists. 


## Changes

See Changes.md

## Todo

- Make a Ansible module of it
- Implement ID output
- Allow configuration files instead of switches
- Create high level functions:
  - update or delete Objects without fetching ids first
  - Okapi Module management
  - Fetch and display API information via /_/proxy/modules/mod-xyz-1.2.3
  - Automatic Id resolution (resolve someId to someObject and display it)
